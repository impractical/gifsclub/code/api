package api

import "darlinggo.co/api"

type Response struct {
	Collections []Collection       `json:"collections,omitempty"`
	GIFs        []GIF              `json:"gifs,omitempty"`
	Files       []File             `json:"files,omitempty"`
	Errors      []api.RequestError `json:"errors,omitempty"`
	Code        int                `json:"-"`
}
